<?php

#Database Connection
class database extends SQLite3
{
    function __construct()
    {
        $this->open('minidebconf.db');
    }
}
$db = new database();
if (!$db)
{
    echo $db->lastErrorMsg();
}
else
{
    $sql =<<<EOF
    CREATE TABLE registration
    (NAME          TEXT    NOT NULL,
    EMAIL         TEXT    NOT NULL,
    ORG           TEXT,
    CITY          TEXT,
    LAP           INT,
    ACCOM         INT,
    TSHIRT        TEXT,
    ARRIVAL       TEXT,
    DEPARTURE     TEXT,
    REGTIME       TEXT);
EOF;
    $ret = $db->exec($sql);
}
$name=$email=$org=$city=$prearrival=$predeparture="";
$lap=$accom=0;
$nameerror = $emailerror = $arrivalerror = $departureerror = $orgerror = $cityerror = "";
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $myDateTime = new DateTime(Date(''), new DateTimeZone('GMT'));
    $myDateTime->setTimezone(new DateTimeZone('Asia/Kolkata'));
    $date=$myDateTime->format('Y-m-d H:i:s');
    if (empty($_POST['name']))
    {
        $nameerror = "Required Field";
    }
    else
    {
        $name = $_POST['name'];
        if(!preg_match('/^[a-zA-Z]+[0-9]*[\. ,]*[a-zA-Z0-9]*$/',$name))
        {
            $nameerror = "Name must start with a letter and can contain only alphanumerics, spaces, periods and commas";
        }
    }
    if (empty($_POST['email']))
    {
        $emailerror = "Required Field";
    }
    else
    {
        $email = $_POST['email'];
        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
        {
            $emailerror = "Invalid Format";
        }
    }
    $org = $_POST['org'];
    if(!preg_match('/$^|^[a-zA-Z]+[0-9]*[\. ,]*[a-zA-Z0-9]*$/',$org))
    {
        $orgerror = "Organization name must start with a letter and can contain only alphanumerics, spaces, periods and commas";
    }
    $city = $_POST['city'];
    if(!preg_match('/$^|^[a-zA-Z]+[0-9]*[\. ,]*[a-zA-Z0-9]*$/',$city))
    {
        $cityerror = "City name must start with a letter and can contain only alphanumerics, spaces, periods and commas";
    }
    $tshirt = $_POST['tshirt'];
    $prearrival = $_POST['arrival'];
    $predeparture = $_POST['departure'];
    if (!preg_match('/^$|(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d/',$_POST['arrival']))
    {
        $arrivalerror = "Incorrect Format - dd-mm-yyyy";
    }
    else
    {
        if (empty($_POST['departure']))
        {
            $departureerror = "If arrival is specified, departure also should be specified";
        }        
        $arrival = date('Y-m-d', strtotime($_POST['arrival']));
    }

    if (!preg_match('/^$|(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d/',$_POST['departure']))
    {
        $departureerror = "Incorrect Format - dd-mm-yyyy";
    }
    else
    {
        if (empty($_POST['arrival']))
        {
            $arrivalerror = "If departure is specified, arrival also should be specified";
        }        
        $departure = date('Y-m-d', strtotime($_POST['departure']));
    }
    $f = date('Y-m-d',strtotime('2014-10-15'));
    $t = date('Y-m-d',strtotime('2014-10-20'));
    if ($arrival < $f or $arrival > $t)
        $arrivalerror = "Date should be between 15th and 20th October, 2014";
    if ($departure < $f or $departure > $t)
        $departureerror = "Date should be between 15th and 20th October, 2014";
    else
    {
        if ($departure < $arrival)
            $departureerror ="Date of departure should be after arrival";
    }
    if (isset($_POST['laptop']))
    {
        $lap = 1;
    }
    else
    {
        $lap = 0;
    }
    if (isset($_POST['accom']))
    {
        $accom = 1;
    }
    else
    {
        $accom = 0;
    }
    if ($nameerror=="" && $emailerror=="" && $arrivalerror=="" && $departureerror=="" && $orgerror =="" && $cityerror=="")
    {
        $sql="INSERT INTO `registration` VALUES ('$name','$email','$org','$city','$lap','$accom','$tshirt','$arrival','$departure','$date')";

        $ret = $db->exec($sql);
        if($ret)
        {
            $db->close();
            header('location:return.html');
        }
    }
}        


?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <title>MiniDebConf Registration</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- CSS  -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/datepickr.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
.error
{
    color:#FF0000;
    margin-left:2%;
}
        </style>
        <!-- Javascript -->
        <script type='text/javascript' src='assets/js/datepickr.js'></script>
    </head>

    <body>
        <div class="topHeaderSection">		
            <div class="container">
                <div class="header"></div>			
                <div class="navbar-header">
                    <a href="/"><img src="images/logo.png" alt="Debutsav ''14, Kerala" /></a>
                </div>
            </div>
        </div>

        <h1 style="margin-left:10%">DebUtsav - Delegate Registraion</h1>
        <div class="row">
            <div style="margin-left:10%">
                <form id="regform" class="form-signin" style="" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="row">
                        <input id="name" name="name" type="text" placeholder="Name *" required onblur="namevalidate()" value="<?php echo $name; ?>"/><span id = "nameerror" class="error col-md-6"><?php echo $nameerror; ?></span>
                    </div>
                    <div class="row">
                        <input name="email" id="email" type="email" required placeholder="Email *" onblur="emailvalidate()" value="<?php echo $email; ?>"/><span id = "emailerror" class="error"><?php echo $emailerror; ?></span>
                    </div>
                    <div class="row">
                        <input name="org" type="text" id="org" placeholder="Organization" onblur="orgvalidate()" value="<?php echo $org;?>" /><span id = 'orgerror' class="error"><?php echo $orgerror; ?></span>
                    </div>
                    <div class="row">
                        <input name="city" type="text" id="city" placeholder="City" onblur = "cityvalidate()"value="<?php echo $city; ?>"/><span id = 'cityerror' class="error"><?php echo $cityerror; ?></span>
                    </div>
                    <div class="row">
                        <label>Laptop : <input name="laptop" type="checkbox" <?php if($lap==1){echo 'checked="checked"';} ?>/></label>
                    </div>
                    <div class="row">
                        <label>Accomodation : <input name="accom" type="checkbox"/></label>
                    </div>
                    <div class="row">
                        <select name="tshirt">
                            <option>None</option>
                            <option>Small</option>
                            <option>Medium</option>
                            <option>Large</option>
                            <option>Extra Large</option>
                        </select>
                    </div>
                    <div id ="a" class="row">
                        <input name="arrival" type="text" id="arrival" placeholder="Arrival Date (dd-mm-yyyy)" id="arrival" onblur = "arrivalvalidate()" value="<?php echo $prearrival; ?>"/><span id = "arrivalerror" class="error"><?php echo $arrivalerror; ?></span>
                    </div>
                    <div class="row" id="b">
                        <input name="departure" type="text" id= "departure" placeholder="Departure Date (dd-mm-yyyy)" id = "departure" onblur="departurevalidate()" value="<?php echo $predeparture; ?>"/><span id = "departureerror" class="error"><?php echo $departureerror; ?></span>
                    </div>
                    <button type="submit" class="btn">Submit</button>
                </form>
            </div>
        </div>
<script>
new datepickr('arrival', {
'dateFormat': 'd-m-Y'
});
new datepickr('departure', {
'dateFormat': 'd-m-Y'
});
</script>
<script type="text/javascript">
function namevalidate()
{
    var $name = document.getElementById('name').value;
    if ($name=="")
    {
        document.getElementById('nameerror').innerHTML = "Required"; 
    }
    else
    {
        if (!$name.match(/^[a-zA-Z]+[0-9]*[\. ,]*[a-zA-Z0-9]*$/))       
        {
            document.getElementById('nameerror').innerHTML = "Name must start with a letter and can contain only alphanumerics, spaces, periods and commas"; 
        }
        else
        {
            document.getElementById('nameerror').innerHTML = ""; 
        }
    }
}

function emailvalidate()
{
    var $email = document.getElementById('email').value;
    if ($email=="")
    {
        document.getElementById('emailerror').innerHTML = "Required"; 
    }
    else
    {
        if (!$email.match(/([\w\-]+\@[\w\-]+\.[\w\-]+)/))
        {
            document.getElementById('emailerror').innerHTML = "Invalid Format"; 
        }
        else
        {
            document.getElementById('emailerror').innerHTML = ""; 
        }
    }
}
function orgvalidate()
{
    var $org = document.getElementById('org').value;
    if (!$org.match(/$^|^[a-zA-Z]+[0-9]*[\. ,]*[a-zA-Z0-9]*$/))
    {
        document.getElementById('orgerror').innerHTML = "Organization name must start with a letter and can contain only alphanumerics, spaces, periods and commas";
    }
    else
    {
        document.getElementById('orgerror').innerHTML = "";
    }
}
function cityvalidate()
{
    var $city = document.getElementById('city').value;
    if (!$city.match(/$^|^[a-zA-Z]+[0-9]*[\. ,]*[a-zA-Z0-9]*$/))
    {
        document.getElementById('cityerror').innerHTML = "City name must start with a letter and can contain only alphanumerics, spaces, periods and commas";
    }
    else
    {
        document.getElementById('cityerror').innerHTML = "";
    }
}
function arrivalvalidate()
{
    var $arrival = document.getElementById('arrival').value;
    if (!$arrival.match(/^$|(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d/))
    {
        document.getElementById('arrivalerror').innerHTML = "Incorrect Format - dd-mm-yyyy";
    }
    else
    {
        from = $arrival.split("-");
        f = new Date(from[2], from[1] - 1, from[0]);
        $startdate = new Date("2014","9","15");
        $enddate = new Date("2014","9","20");
        if (f < $startdate || f > $enddate)
        {
            document.getElementById('arrivalerror').innerHTML = "Date should be between 15th and 20th October, 2014";
        }
        else
        {
            document.getElementById('arrivalerror').innerHTML = "";
        }
    }
}
function departurevalidate()
{
    var $departure = document.getElementById('departure').value;
    if (!$departure.match(/^$|(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d/))
    {
        document.getElementById('departureerror').innerHTML = "Incorrect Format - dd-mm-yyyy";
    }
    else
    {

        var $arrival = document.getElementById('arrival').value;
        to = $departure.split("-");
        t = new Date(to[2], to[1] - 1, to[0]);
        $startdate = new Date("2014","9","15");
        $enddate = new Date("2014","9","20");
        if (t < $startdate || t > $enddate)
        {
            document.getElementById('departureerror').innerHTML = "Date should be between 15th and 20th October, 2014";
        }
        else
        {
            if ($arrival !="")
            {
                from = $arrival.split("-");
                f = new Date(from[2], from[1] - 1, from[0]);
                if (t < f)
                {
                    document.getElementById('departureerror').innerHTML = "Date of departure should be after arrival";
                }
                else
                {
                    document.getElementById('departureerror').innerHTML = "";
                }
            }
            else
            {
                document.getElementById('departureerror').innerHTML = "";
            }
        }
    }
}
</script>

    </body>
</html>



